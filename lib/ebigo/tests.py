# Copyright 2016, 2017 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, unittest, gameobjects

class TestGameObjects(unittest.TestCase):
    def setUp(self):
        self.screenW = 50
        self.screenH = 50
        self.cellRect = pygame.Rect(50, 50, 12, 12)
        self.sprite = gameobjects.WallMazeSprite("Test Sprite", (0, 0, 8, 8))
        self.sprite.center = self.cellRect.topleft
        self.startX = self.sprite.x
        self.startY = self.sprite.y

    def test_moveInCenter(self):
        for dist in range(0, 16):
            for x in range(0, self.screenW):
                for y in range(0, self.screenH):
                    for direct in (gameobjects.ANYORTHOGONAL):
                        self.sprite.topleft = (x, y)
                        newRect, newDirect = self.sprite.getInCenter(self.cellRect, direct, dist)
                        self.sprite.clamp(newRect)
                        if newDirect == direct:
                            if dir in (gameobjects.LEFT, gameobjects.RIGHT):
                                self.assertEqual(self.sprite.centery, self.cellRect.centery)
                            elif dir in (gameobjects.UP, gameobjects.DOWN):
                                self.assertEqual(self.sprite.centerx, self.cellRect.centerx)

if __name__ == '__main__':
    unittest.main()
