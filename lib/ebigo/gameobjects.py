# Copyright 2015, 2016, 2017, 2018 Eric Duhamel

# This file is part of EBIGO.

# EBIGO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# EBIGO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with EBIGO.  If not, see <http://www.gnu.org/licenses/>.

import copy, time, math
import backend

class PlayObject(backend.GameObject):
    """An object representing one play of a game.

    A play object is mainly responsible for creating and destroying
    pawn objects and field objects, and managing their interactions."""
    def __init__(self, width=0, height=0):
        super(PlayObject, self).__init__((0, 0, width, height))
#        self.setScale(width, height)
        self.scale = 1
        self.allObjs = []
        self.allFieldObjs = []
        self.WRAP = 'wrap'
        self.BOUNCE = 'bounce'
        self.STOP = 'stop'
        self.SLIDE = 'slide'
        self.timers = []
        self.NOEVENT = 0
        self.FIELDEVENT = 1
        self.roomnum = 0
        self.worldwidth = 3
        return None

    def getScale(self, windowwidth, windowheight):
        if self.width == None:
            self.width = windowwidth
        if self.height == None:
            self.height = windowheight
        # Determine scale based on difference
        if windowwidth > 0 and windowheight > 0:
            if windowheight < (self.height*2):
                scale = 1
            elif windowheight < (self.height*3):
                scale = 2
            else:
                scale = 3
        else:
            scale = 1
        return scale

    # TODO: setScale is a duplicate of getScale; deprecate
    def setScale(self, wwidth, wheight):
        if self.width == 0:
            self.width = wwidth
        if self.height == 0:
            self.height = wheight
        # Determine scale based on difference
        if wwidth > 0 and wheight > 0:
            if wheight < (self.height*2):
                self.scale = 1
            elif wheight < (self.height*3):
                self.scale = 2
            else:
                self.scale = 3
        else:
            self.scale = 1

    def addObject(self, object, field=None, index=None):
        object.field = field
        if index is None:
            self.allObjs.append(object)
        else:
            self.allObjs.insert(index, object)
        return object

    def removeObject(self, object):
        if object in self.allObjs:
            self.allObjs.remove(object)

    def countObjects(self, name):
        count = 0
        for object in self.allObjs:
            if object.name == name:
                count += 1
        return count

    def getAllObjs(self):
        return self.allObjs

    def getAllObjDicts(self):
        allObjDicts = []
        for obj in self.allObjs:
            allObjDicts.append(obj.getDict())
        return allObjDicts

    def getAllObjStates(self):
        allObjStates = []
        for obj in self.allObjs:
            for objState in obj.states:
                allObjStates.append(
                    {'name': obj.name + '_' + objState['name'],
                     'size': objState['size'],
                     'steps': objState['steps']})
        return allObjStates

    def getAllObjNames(self):
        allObjNames = []
        for obj in self.allObjs:
            allObjNames.append(obj.getStats())
#                {'name': obj.fullname,
#                {'name': obj.name,
#                 'size': obj.height,
#                 'steps': obj.steps}
#            )
        return allObjNames

    def addFieldObject(self, fieldobject):
        self.allFieldObjs.append(fieldobject)
        return fieldobject

    def getField(self, index=None):
        if index == None:
            roomnum = self.roomnum
        else:
            roomnum = index
#        if len(self.allFieldObjs) > index:
#            return self.allFieldObjs[index]
        if len(self.allFieldObjs) > roomnum:
            return self.allFieldObjs[roomnum]
        else:
            return self.allFieldObjs[0]

    def getAllFieldObjs(self):
        """Get all the play fields in the game.

        This is named similarly to getAllObjs(), but is used more like
        getAllObjStates(). Should fix that to be more clear."""
        allDicts = []
        for field in self.allFieldObjs:
            dict = {}
#            dict['name'] = field.name
            dict['maps'] = field.boxmaps
            dict['width'] = field.width
            dict['height'] = field.height
            dict['tilewidth'] = field.TILEWIDTH
            dict['tileheight'] = field.TILEHEIGHT
            allDicts.append(dict)
        return allDicts

    def setTimer(self):
        self.timers.append(time.time())
        return len(self.timers)-1

    def getTimer(self, index):
        if len(self.timers) > index:
            return time.time() - self.timers[index]
        else:
            return None

    def resetTimer(self, index):
        if len(self.timers) > index:
            self.timers[index] = time.time()
            return True
        else:
            return False

    def controlgame(self, controldicts):
        for playerNum, object in enumerate(self.allObjs):
            # TODO: gross; make this part conform to Player/Intent standard
            if len(controldicts) > playerNum:
                playerDirect = controldicts[playerNum]['directions'][self.FIRST]
                if controldicts[playerNum]['triggers'][self.FIRST]:
                    object.doTrigger(1)
            else:
                playerDirect = None
            # Move objects based on player input
#            if (playerDirect in self.ANYORTHOGONAL or
#                    playerDirect == self.NODIRECT):
            direct = playerDirect
            object.direction = object.doDirection(direct)

    def playgame(self, edgebehavior=None, boundrect=None, xparams={}):
        """Perform a step of game play with default generic physics."""
        for n, object in enumerate(self.allObjs):
            # move objects based on their deltas
            if object.dx != 0 or object.dy != 0:
                if not object.killed:
                    object.drift()
            # determine the rectangle the object is confined inside
            if object.field == None:
                boundRect = self.getrect()
            else:
                boundRect = object.field
            # resolve collisions with playfield blocks
            field = self.getField(self.roomnum)
            direct = object.direction
            blocks = field.getBlocks(object, object.dx, object.dy)
            for block in blocks:
                if block != None:
                    object.moveoutside(
                        block, self.DIRECT2OPPOSITE[object.direction]
                    )
            # resolve collisions with playfield boundary
            exitdirect = object.moveinside(boundRect, object.edginess)
            # TODO: the following should be contained in method gonextroom()
            if exitdirect and n == 0:
                self.roomnum = self.getnextroom(
                    self.roomnum, exitdirect, self.worldwidth)
                return self.FIELDEVENT

    def getnextroom(self, roomnum, direct, width):
            nextroom = roomnum
            if direct == self.LEFT:
                nextroom -= 1
            elif direct == self.UP:
                nextroom -= width
            elif direct == self.RIGHT:
                nextroom += 1
            elif direct == self.DOWN:
                nextroom += width
            if nextroom < 0:
                nextroom = len(self.allFieldObjs)-1
            if nextroom > len(self.allFieldObjs):
                nextroom = 0
            return nextroom

    def getObjectStats(self):
        """Return a dictionary describing objects that will be in the game."""
        objDicts = []
        for object in self.allObjs:
            objDicts.append(object.getStats())
        return objDicts


class PawnObject(backend.GameObject):
    """An object to represent players, enemies, and missiles.

    A pawn object needs only a few methods to operate; doDirection
    and doTrigger to respond to player intentions, setDirection
    to change which way it is pointing,
    """
    def __init__(self, name, x=160, y=120, width=16, height=16):
        self.name = name
        self.type = name
        self.step = 0
        self.speed = 4.0
        self.xparams = {}
        super(PawnObject, self).__init__((x, y, width, height))
        self.direction = 0
        self.dx = 0.0
        self.dy = 0.0
        self.extraStats = {}
        self.TOPLEFT = 'top-left'
        self.MIDTOP = 'middle-top'
        self.TOPRIGHT = 'top-right'
        self.MIDRIGHT = 'middle-right'
        self.BOTTOMRIGHT = 'bottom-right'
        self.MIDBOTTOM = 'middle-bottom'
        self.BOTTOMLEFT = 'bottom-left'
        self.MIDLEFT = 'middle-left'
        self.MIDDLE = 'middle'
        self.anchor = self.TOPLEFT
        # Some constants that define playfield edge behavior
        self.WRAP = 'wrap'
        self.BOUNCE = 'bounce'
        self.STOP = 'stop'
        self.SLIDE = 'slide'
        self.edginess = self.WRAP
        self.killed = False

    def doDirection(self, direction, speed=1, movement=None):
        max = 16
        if direction in self.ANYLEFT:
            if movement == "direct":
                self.dx = -speed
            elif movement == "turn":
                self.turn(speed, self.LEFT)
            elif self.dx > -max:
                self.dx -= speed
        elif direction in self.ANYRIGHT:
            if movement == "direct":
                self.dx = speed
            elif movement == "turn":
                self.turn(speed, self.RIGHT)
            elif self.dx < max:
                self.dx += speed
        if direction in self.ANYUP:
            if movement == "direct":
                self.dy = -speed
            elif self.dy > -max:
                self.dy -= speed
        elif direction in self.ANYDOWN:
            if movement == "direct":
                self.dy = speed
            elif self.dy < max:
                self.dy += speed
        if direction not in self.ANYHORIZONTAL or direction == self.NODIRECT:
            if movement == "direct":
                self.dx = 0
        if direction not in self.ANYVERTICAL or direction == self.NODIRECT:
            if movement == "direct":
                self.dy = 0
        return self.NODIRECT

    def doTrigger(self, index):
        """Perform an action based on a player intention

        Intentions are numbered from 1 to an indefinite maximum"""
        return None

    def turn(self, degrees, direct=None):
        if not direct:
            self.direction = degrees
        elif direct == self.LEFT:
            self.direction += degrees
        elif direct == self.RIGHT:
            self.direction -= degrees
        while self.direction < 1:
            self.direction += 360
        while self.direction > 360:
            self.direction -= 360

    def drift(self, dx=None, dy=None):
        if dx != None:
            x = self.x + dx
        else:
            x = self.x + self.dx
        if dy != None:
            y = self.y + dy
        else:
            y = self.y + self.dy
        super(PawnObject, self).move(x, y)

    def moveoutside(self, rect, direction=None):
        if direction == self.LEFT or self.dx > 0:
            self.right = rect.left
            self.dx = 0
        elif direction == self.UP or self.dy > 0:
            self.bottom = rect.top
            self.dy = 0
        elif direction == self.RIGHT or self.dx < 0:
            self.left = rect.right
            self.dx = 0
        elif direction == self.DOWN or self.dy < 0:
            self.top = rect.bottom
            self.dy = 0
        return None

    def moveinside(self, rect, method=None):
        """Move inside the provided rectangle

        Several methods of moving are defined. Moves in place. Return
        None or a direction describing which edge the object collided
        with."""
        if not rect.contains(self):
            if method in (None, self.WRAP):
                if self.centerx < rect.x:
                    self.centerx = (rect.x + rect.width)
                    return self.LEFT
                elif self.centery < rect.y:
                    self.centery = (rect.y + rect.height)
                    return self.UP
                elif self.centery > (rect.y + rect.height):
                    self.centery = rect.y
                    return self.DOWN
                elif self.centerx > (rect.x + rect.width):
                    self.centerx = rect.x
                    return self.RIGHT
            elif method == self.STOP:
                if self.left < rect.left:
                    self.left = rect.left
                    return self.LEFT
                elif self.top < rect.top:
                    self.top = rect.top
                    return self.UP
                elif self.bottom > rect.bottom:
                    self.bottom = rect.bottom
                    return self.DOWN
                elif self.right > rect.right:
                    self.right = rect.right
                    return self.RIGHT
        else:
            return None

    def hitbound(self, direction):
        return False

    def hitBlock(self):
        return False

    def destroy(self):
        self.killed = True


class FieldObject(backend.GameObject):
    """A rectangular playing field.

    A field object provides a rectangle within which to confine pawn
    objects and a map of smaller rectangles representing terrain.
    """
    def __init__(self, x, y, width, height, tilemap=None):
        if tilemap:
            self.columns = len(tilemap[0])
            self.rows = len(tilemap)
        else:
            self.columns = 1
            self.rows = 1
        self.TILEWIDTH = width / self.columns
        self.TILEHEIGHT = height / self.rows
        self.TILEMAP = tilemap
        self.boxmaps = []
        super(FieldObject, self).__init__((x, y, width, height))

    def addBoxMap(self, name, map, tilewidth, tileheight, mapwidth=0):
        if mapwidth > 0:
            self.columns = mapwidth
            self.rows = len(map)/mapwidth
        else:
            self.columns = len(map[0])
            self.rows = len(map)
        self.TILEWIDTH = self.width / self.columns
        self.TILEHEIGHT = self.height / self.rows
        field = self.getrect(
            self.x,
            self.y,
            tilewidth*self.columns,
            tileheight*self.rows
        )
        mapdict = {
            'name': name,
            'map': map,
            'field': field,
            'tilewidth': tilewidth,
            'tileheight': tileheight,
            'mapwidth': self.columns  # Huh?!
        }
        self.boxmaps.append(mapdict)

    def getAllBoxMaps(self):
        return self.boxmaps

    def getBoxAtPixel(self, x, y, index=0):
        if len(self.boxmaps) > index:
            boxmap = self.boxmaps[index]['map']
            mapwidth = self.boxmaps[index]['mapwidth']
            maplength = len(boxmap)
            boxwidth = self.boxmaps[index]['tilewidth']
            boxheight = self.boxmaps[index]['tileheight']
        else:
            boxmap = self.boxmaps[0]
        tarX = x-self.x
        tarY = y-self.y
        cellX = int((x - self.x) / boxwidth)
        cellY = int((y - self.y) / boxheight)
        boxX = self.x + (cellX * boxwidth)
        boxY = self.y + (cellY * boxheight)
        boxRect = self.getrect(boxX, boxY, boxwidth, boxheight)
        if (cellX > -1 and cellY > -1
                and cellX < mapwidth
                and (cellY*mapwidth)+cellX < maplength):
            boxIndex = (cellY*mapwidth)+cellX
            boxValue = boxmap[boxIndex]
        else:
            boxValue = 0
        return boxRect, boxValue

    def getBlockAtPixel(self, x, y, index):
        box, value = self.getBoxAtPixel(x, y, index)
        if value != 0:
            return box
        else:
            return None

    def getBlocks(self, rect, deltax, deltay, index=0):
        blocks = []
        if deltax < 0:
            blocks.append(
                self.getBlockAtPixel(rect.left, rect.top, index)
            )
            blocks.append(
                self.getBlockAtPixel(rect.left, rect.bottom-1, index)
            )
        elif deltay < 0:
            blocks.append(
                self.getBlockAtPixel(rect.left, rect.top, index)
            )
            blocks.append(
                self.getBlockAtPixel(rect.right-1, rect.top, index)
            )
        elif deltax > 0:
            blocks.append(
                self.getBlockAtPixel(rect.right, rect.top, index)
            )
            blocks.append(
                self.getBlockAtPixel(rect.right, rect.bottom-1, index)
            )
        elif deltay > 0:
            blocks.append(
                self.getBlockAtPixel(rect.left, rect.bottom, index)
            )
            blocks.append(
                self.getBlockAtPixel(rect.right-1, rect.bottom, index)
            )
        return blocks

    def createMap(self, columns, rows, floodvalue):
        boxMap = []
        for boxH in range(int(rows)):
            boxMapRow = []
            for boxW in range(int(columns)):
                boxMapRow.append(floodvalue)
            boxMap.append(boxMapRow)
        return boxMap

    def decorateMap(self, plainmap, plainval):
        # Set up an array of choices so that common decorations are randomly
        # chosen more often than rare decorations
        boxChoices = []
        decor = {11: 48, 18: 16, 17: 2, 16: 1}
        for d in decor:
            for i in range(decor[d]):
                boxChoices.append(d)
        # Replace boxes of map with decorations
        decoratedMap = copy.copy(plainmap)
        for i in range(len(plainmap)):
            for box in range(len(plainmap[i])):
                if plainmap[i][box] == plainval:
                    decoratedMap[i][box] = self.getChoice(tuple(boxChoices))
        return decoratedMap

    def addFeature(self, boxmap, farray, x, y):
        i = 0
        # Complicated while loop to paste the pattern in farray
        # at the position described by x, y
        while y+i < len(boxmap) and i < len(farray):
            j = 0
            while x+j < len(boxmap[y]) and j < len(farray[i]):
                boxmap[y+i][x+j] = farray[i][j]
                j += 1
            i += 1
        return boxmap
