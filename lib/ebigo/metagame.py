# Copyright 2016, 2017, 2018 Eric Duhamel

# This file is part of EBIGO.

# EBIGO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# EBIGO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with EBIGO.  If not, see <http://www.gnu.org/licenses/>.

import os
import controls
import graphics
import sounds
import backend

class MetaGame(backend.MetaObj):
    def __init__(self, game, winwidth, winheight):
        super(MetaGame, self).__init__(game)
        windowed = graphics.WINDOWED
        self.screenMan = graphics.Screen(
            winwidth, winheight,
            "EBIGO Game", windowed,
            False
        )
        self.playerInt = controls.ControlMap()
        #self.audioMan = sounds.Sounds()
        objects = self.game.getAllObjs()
        self.addobjects(objects)
        self.redraw = True

    def run(self):
        # quit running if user sent an exit command
        if self.playerInt.process_events() == -1:
            done = True
        else:
            done = False
        # Get player input from controls
        playerIntents = self.playerInt.getDicts()
        # Run the game
        self.game.controlgame(playerIntents)
        redrawEvent = self.game.playgame()
        if redrawEvent != None:
            # any non-zero return represents a game state change
            # that needs to be redrawn
            self.redraw = True
        # Draw all the graphics
        if self.redraw:
            # draw the playing field if necessary
            if len(self.game.getAllFieldObjs()):
                self.screenMan.blank()
                field = self.game.getField()
                if field != None:
                    for tilemap in field.getAllBoxMaps():
                        self.screenMan.drawTileMap(
                            tilemap['name'], tilemap['field'],
                            tilemap['map'], tilemap['mapwidth'],
                            graphics.BACKGROUND
                        )
            # add and remove any objects that were changed
            objects = self.game.getAllObjs()
            self.addobjects(objects)
            # set flag to False and wait for next event
            self.redraw = False
        # every frame redraw the sprites
        self.screenMan.updateSprites()
        self.screenMan.updateScreen()
        # limit frames per second
        self.takeTime(self.FPS)
        return done

    def takeTime(self, framesPerSecond):
        self.FPSCLOCK.tick(framesPerSecond)

    def loadspritesheets(self, imagelist=()):
        """Load sprite sheets from disk.

        Games should override this method in order to supply custom
        dictionaries representing the actual sprite sheets used."""
        for gameObj in imagelist:
            name = gameObj['name']
            xparams = {}
            for param in gameObj:
                xparams[param] = gameObj[param]
            xparams['rotate'] = True
            self.screenMan.addSpriteSheet(
                os.path.join(self.DATADIR, gameObj['name'] + '.png'),
                gameObj['name'],
                gameObj['width'],
                gameObj['height'],
                gameObj['steps'], self.game.scale, xparams
            )

    def loadtilesheets(self, imagelist=(), scale=1):
        """Tile-sets are used to draw foregrounds and backgrounds."""
        xparams = {'scale': scale,}
        for fieldObj in imagelist:
            boxmaps = fieldObj['maps']
            if len(boxmaps) > 0:
                for boxmap in boxmaps:
                    self.screenMan.addTileSet(
                        self.DATADIR + boxmap['name'] + '.png',
                        boxmap['name'],
                        fieldObj['tilewidth'],
                        fieldObj['tileheight'],
                        xparams
                    )

    def addobjects(self, objects):
        """Add objects for the graphics engine to draw."""
        if self.game != None:
            self.screenMan.removeAllImages()
            for object in objects:
                self.screenMan.addAnimatedImage(object)
        return None

    def getMaps(self):
        """Return all of the currently used box-maps.

        Box-maps are used to build composite backgrounds and
        foregrounds using images from tilesheets."""
        return None
