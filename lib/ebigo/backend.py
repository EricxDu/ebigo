# Copyright 2015, 2016, 2017, 2018 Eric Duhamel

# This file is part of EBIGO.

# EBIGO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# EBIGO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with EBIGO.  If not, see <http://www.gnu.org/licenses/>.

import pygame
import random
random.seed()

class GameObject(pygame.Rect):
    def __init__(self, rect):
        self.x = float(rect[0])
        self.y = float(rect[1])
        self.width = rect[2]
        self.height = rect[3]
        self.FIRST = 0
        self.SECOND = 1
        self.THIRD = 2
        self.FOURTH = 3
        self.DIRECTLEFT = 360
        self.DIRECTUP = 270
        self.DIRECTRIGHT = 180
        self.DIRECTDOWN = 90
        self.LEFT = 360
        self.UPLEFT = 315
        self.UP = 270
        self.UPRIGHT = 225
        self.RIGHT = 180
        self.DOWNRIGHT = 135
        self.DOWN = 90
        self.DOWNLEFT = 45
        self.NODIRECT = 0
        self.CLOCKDIR1 = 240
        self.CLOCKDIR2 = 210
        self.CLOCKDIR3 = 180
        self.CLOCKDIR4 = 150
        self.CLOCKDIR5 = 120
        self.CLOCKDIR6 = 90
        self.CLOCKDIR7 = 60
        self.CLOCKDIR8 = 30
        self.CLOCKDIR9 = 360
        self.CLOCKDIR10 = 330
        self.CLOCKDIR11 = 300
        self.CLOCKDIR12 = 270
        self.ANYLEFT =  (self.LEFT, self.UPLEFT, self.DOWNLEFT)
        self.ANYUP =    (self.UP, self.UPLEFT, self.UPRIGHT)
        self.ANYDOWN =  (self.DOWN, self.DOWNLEFT, self.DOWNRIGHT)
        self.ANYRIGHT = (self.RIGHT, self.UPRIGHT, self.DOWNRIGHT)
        self.ANYDIAGONAL = (self.UPRIGHT, self.DOWNRIGHT,
                            self.DOWNLEFT, self.UPLEFT)
        self.DIRECT2ANY = {
            self.LEFT: self.ANYLEFT,
            self.UP: self.ANYUP,
            self.DOWN: self.ANYDOWN,
            self.RIGHT: self.ANYRIGHT,
            self.NODIRECT: self.ANYLEFT
        }
        self.DIAGONAL2DIRECTS = {
            self.UPLEFT: (self.UP, self.LEFT),
            self.UPRIGHT: (self.UP, self.RIGHT),
            self.DOWNRIGHT: (self.DOWN, self.RIGHT),
            self.DOWNLEFT: (self.DOWN, self.LEFT),
        }
        self.DIAG2Y = {
            self.UPLEFT: self.UP,
            self.UPRIGHT: self.UP,
            self.DOWNRIGHT: self.DOWN,
            self.DOWNLEFT: self.DOWN,
        }
        self.DIAG2X = {
            self.UPLEFT: self.LEFT,
            self.UPRIGHT: self.RIGHT,
            self.DOWNRIGHT: self.RIGHT,
            self.DOWNLEFT: self.LEFT,
        }
        self.DIAGONAL2ORTHOGONAL = {
            self.UPLEFT: self.UP,
            self.UPRIGHT: self.RIGHT,
            self.DOWNRIGHT: self.DOWN,
            self.DOWNLEFT: self.LEFT,
        }
        self.ANYORTHOGONAL = (self.LEFT, self.UP, self.DOWN, self.RIGHT)
        self.ANYHORIZONTAL = (self.LEFT, self.UPLEFT, self.DOWNLEFT,
                              self.RIGHT, self.UPRIGHT, self.DOWNRIGHT)
        self.ANYVERTICAL = (self.UP, self.UPLEFT, self.UPRIGHT,
                            self.DOWN, self.DOWNLEFT, self.DOWNRIGHT)
        self.ANYDIRECT = (
            self.LEFT,
            self.UPLEFT,
            self.UP,
            self.UPRIGHT,
            self.RIGHT,
            self.DOWNRIGHT,
            self.DOWN,
            self.DOWNLEFT,
            self.NODIRECT
        )
        self.DIRECT2OPPOSITE = {
            self.LEFT: self.RIGHT,
            self.UPLEFT: self.DOWNRIGHT,
            self.UP: self.DOWN,
            self.UPRIGHT: self.DOWNLEFT,
            self.RIGHT: self.LEFT,
            self.DOWNRIGHT: self.UPLEFT,
            self.DOWN: self.UP,
            self.DOWNLEFT: self.UPRIGHT,
            self.NODIRECT: self.NODIRECT
        }
        self.DIRECT2VERBAL = {
            self.LEFT: 'left',
            self.UP: 'up',
            self.DOWN: 'down',
            self.RIGHT: 'right'
        }

    def getChoice(self, choices):
        return random.choice(choices)

    def getChance(self, denominator, numerator):
        """Return whether you beat the odds.

        Enter the odds as See-Threepio tells them to you, i.e. 3720 to
        1. Remember the denominator is the larger number, and the
        numerator is the smaller. They are reversed above from the
        normal order compared to when you are typing out a fraction."""
        # Generate a random number as high as the larger number
        randomNum = random.randint(0, denominator)
        # Return True if the number is less than the smaller number
        # i.e. you beat the odds!
        return randomNum < numerator

    def getNumber(self, high, low=0):
        return random.randint(low, high)

    def move(self, x, y):
        self.x = x
        self.y = y

    def getrect(self, x=None, y=None, width=None, height=None):
        if not x:
            rx = self.x
        else:
            rx = x
        if not y:
            ry = self.y
        else:
            ry = y
        if not width:
            rw = self.width
        else:
            rw = width
        if not height:
            rh = self.height
        else:
            rh = height
        return pygame.Rect((rx, ry, rw, rh))


class Graphics(object):
    def __init__(self):
        # Establish some basic colors
        # CGA graphics pallete (4-bit, 16 colors)
        # RGBI: 0, 85, 170, 255
        # OFF=0, NORMAL=170, INTENSE=+85
        self.PALETTE = (
            (  0,   0,   0), ( 85,  85,  85),
            (  0,   0, 170), ( 85,  85, 255),
            (  0, 170,   0), ( 85, 255,  85),
            (  0, 170, 170), ( 85, 255, 255),
            (170,   0,   0), (255,  85,  85),
            (170,   0, 170), (255,  85, 255),
            (170,  85,   0), (255, 255,  85),
            (170, 170, 170), (255, 255, 255),
        )
        bgcolor = 0
        self.PAL0 = (
            self.PALETTE[bgcolor],
            self.PALETTE[2],
            self.PALETTE[4],
            self.PALETTE[6],
        )
        self.PAL0I = (
            self.PALETTE[bgcolor],
            self.PALETTE[10],
            self.PALETTE[12],
            self.PALETTE[14],
        )
        self.PAL1 = (
            self.PALETTE[bgcolor],
            self.PALETTE[3],
            self.PALETTE[5],
            self.PALETTE[7],
        )
        self.PAL1I = (
            self.PALETTE[bgcolor],
            self.PALETTE[11],
            self.PALETTE[13],
            self.PALETTE[15],
        )
        # Brighter palette
        # Invent: 0,  64, 128, 192, 255
        # OFF=0, LOW=64, NORMAL=128, INTENSE=+128
        # MEDIUM
        self.BLACK      = (  0,   0,   0)  # 0
        self.NAVY       = (  0,   0, 128)  # 1
        self.GREEN      = (  0, 128,   0)  # 2
        self.TEAL       = (  0, 128, 128)  # 3
        self.MAROON     = (128,   0,   0)  # 4
        self.PURPLE     = (128,   0, 128)  # 5
        self.OLIVE      = (128, 128,   0)  # 6
        self.GRAY       = (128, 128, 128)  # 7
        # BRIGHT
        self.SILVER     = (192, 192, 192)
        self.RED        = (255,   0,   0)
        self.FUCHSIA    = (255,   0, 255)
        self.YELLOW     = (255, 255,   0)
        self.BLUE       = (  0,   0, 255)
        self.LIME       = (  0, 255,   0)
        self.AQUA       = (  0, 255, 255)
        self.WHITE      = (255, 255, 255)
        self.BRITEPAL = (
            self.PALETTE[bgcolor],
            self.LIME,
            self.RED,
            self.YELLOW
        )
        self.BLUEPAL = (
            self.PALETTE[bgcolor],
            self.AQUA,
            self.FUCHSIA,
            self.WHITE
        )


class MetaObj(object):
    def __init__(self, game):
        self.game = game
        self.FPS = 30
        self.FPSCLOCK = pygame.time.Clock()
