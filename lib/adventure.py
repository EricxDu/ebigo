import sys
from ebigo import gameobjects
from ebigo import metagame

class Game(gameobjects.PlayObject):
    def __init__(self, width, height):
        super(Game, self).__init__(width, height)
        # 0  0000  nwse
        # 1  0001  nwsE
        # 2  0010  nwSe
        # 4  0100  nWse
        # 8  1000  Nwse
        # 15 1111  NWSE
        self.player = self.addObject(Guy())
        self.addFieldObject(Room(13))
        self.addFieldObject(Room(13))
        keyroom = self.addFieldObject(Room(14))
        self.addFieldObject(Room(2))
        self.addFieldObject(Room(2))
        self.addFieldObject(Room(8))
        self.addFieldObject(Room(10))
        self.addFieldObject(Room(11))
        wandroom = self.addFieldObject(Room(7))
        self.key = Key()
        self.wand = Wand()
        keyroom.addPawn(self.key)
        wandroom.addPawn(self.wand)

    def getnextroom(self, roomnum, direct, width):
        roomnum = super(Game, self).getnextroom(roomnum, direct, width)
        self.removeObject(self.key)
        self.removeObject(self.wand)
        field = self.getField(roomnum)
        for pawn in field.pawns:
            self.addObject(pawn)
        return roomnum

    def collidetools(self):
        for tool in self.allObjs:
            if tool != self.player:
                if self.player.colliderect(tool):
                    tool.moveoutside(self.player, self.player.direction)

    def playgame(self):
        event = super(Game, self).playgame()
        self.collidetools()
        return event


class Meta(metagame.MetaGame):
    def __init__(self, width, height):
        game = Game(width, height)
        super(Meta, self).__init__(game, width, height)


class Guy(gameobjects.PawnObject):
    def __init__(self):
        super(Guy, self).__init__("guy")
        self.holding = None

    def doDirection(self, direct, speed=12, movement="direct"):
        if direct in self.ANYDIAGONAL:
            direct = self.DIAGONAL2ORTHOGONAL[direct]
        super(Guy, self).doDirection(direct, speed, movement)
        return direct


class Tool(gameobjects.PawnObject):
    def __init__(self, name):
        super(Tool, self).__init__(name, 200, 200, 8, 8)

    def moveoutside(self, rect, direct):
        return super(Tool, self).moveoutside(rect, direct)


class Key(Tool):
    def __init__(self):
        super(Key, self).__init__("key")


class Wand(Tool):
    def __init__(self):
        super(Wand, self).__init__("wand")


class Room(gameobjects.FieldObject):
    """A generic room that can have SENW doors"""
    def __init__(self, closed=0):
        super(Room, self).__init__(0, 0, 320, 240)
        # 1  0001  nwsE
        # 2  0010  nwSe
        # 4  0100  nWse
        # 8  1000  Nwse
        north = (8, 9, 10, 11, 12, 13, 14, 15)
        west = (4, 5, 6, 7,  12, 13, 14, 15)
        south = (2, 3,  6, 7,  10, 11,  14, 15)
        east = (1,  3,  5,  7,  9,  11,  13,  15)
        N=0
        W=0
        S=0
        E=0
        if closed in north:
            N = 1
        if closed in west:
            W = 1
        if closed in south:
            S = 1
        if closed in east:
            E = 1
        boxmap = (
            1, 1, 1, 1, 1, 1, 1,  N, N, N, N, N, N,  1, 1, 1, 1, 1, 1, 1,
            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,

            W, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, E,
            W, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, E,
            W, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, E,
            W, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, E,
            W, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, E,

            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 1,
            1, 1, 1, 1, 1, 1, 1,  S, S, S, S, S, S,  1, 1, 1, 1, 1, 1, 1,
        )
        self.addBoxMap("layer1", boxmap, 32, 32, 20)
        self.pawns = []

    def addPawn(self, pawn):
        self.pawns.append(pawn)


if __name__ == '__main__':
    meta = Meta(640, 480)
    done = False
    while not done:
        done = meta.run()
    sys.exit()
